from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select


class LoginPage():

    @property
    def PAGE_TITLE(self):
        return (By.TAG_NAME, 'h3')
    USERNAME = (By.ID, "id_username")
    PASSWORD = (By.ID, "id_password")
    LOGIN = (By.TAG_NAME, 'button')
    NEW_ACCOUNT = (By.PARTIAL_LINK_TEXT, "Create")
    RESET_PASSWORD = (By.PARTIAL_LINK_TEXT, "Reset")
    INVALID_CREDENTIAL = (
        By.CSS_SELECTOR, "alert alert-danger alert-dismissible")
    LOG_OUT = (By.PARTIAL_LINK_TEXT, "Log out")

    def __init__(self, driver):
        self.driver = driver

    def enter_username(self, username):
        self.driver.find_element(*self.USERNAME).send_keys(username)

    def enter_password(self, password):
        self.driver.find_element(*self.PASSWORD).send_keys(password)

    def click_ok(self):
        self.driver.find_element(*self.LOGIN).click()

    def click_create_account(self):
        self.driver.find_element(*self.NEW_ACCOUNT).click()

    def click_reset_password(self):
        self.driver.find_element(*self.RESET_PASSWORD).click()

    def get_invalid_credential_message(self):
        return self.driver.find_element(*self.INVALID_CREDENTIAL).text

    def log_out_is_visible(self):
        self.driver.find_element(*self.LOG_OUT).is_displayed()

    def click_on_log_out(self):
        self.driver.find_element(*self.LOG_OUT).click()
