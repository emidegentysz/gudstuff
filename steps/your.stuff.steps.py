from behave import given, when, then, step  # pylint: disable=no-name-in-module
import pytest
from selenium import webdriver
import sys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from time import sleep
from pages.your_stuff import YourStuff


@when('user go to Your stuff page')
def step_your_stuff_page(context):
    YourStuff(context.driver).go_to_your_stuff_page()


@then('user can see proper page: "{title}"')
def verify_page_title(context, title):
    assert title == YourStuff(context.driver).get_page_title_text()


@step('user go to Stuff in progress')
def step_stuff_in_progress_page(context):
    YourStuff(context.driver).go_to_stuff_in_progress_page()


@step('user go to Abandoned stuff')
def step_abandoned_stuff_page(context):
    YourStuff(context.driver).go_to_abandoned_stuff_page()


@step('user click on edit option')
def step_update_stuff(context):
    YourStuff(context.driver).go_to_update_stuff_page()


@then('user can see proper message: "{message}"')
def verify_page_message(context, message):
    assert message == YourStuff(context.driver).get_no_stuff_message()


@then('user can see at least one postion on page')
def step_abandoned_stuff_visible(context):
    assert True == YourStuff(context.driver).verify_abandoned_stuff_page()


@then('user can see update page')
def step_update_one_stuff(context):
    assert True == YourStuff(context.driver).verify_update_stuff_page()


@step('user add review: "{review}"')
def step_add_review_stuff(context, review):
    YourStuff(context.driver).add_review_to_stuff(review)


@step('user add rating: "{rating}"')
def step_add_rating_stuff(context, rating):
    YourStuff(context.driver).add_rating_to_stuff(rating)


@step('user click on save')
def step_save_review_stuff(context):
    YourStuff(context.driver).save_review_to_stuff()
