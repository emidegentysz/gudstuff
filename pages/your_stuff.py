from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select


class YourStuff():

    @property
    def PAGE_TITLE(self):
        return (By.PARTIAL_LINK_TEXT, "Your stuff")
    STUFF_IN_PROGRESS = (By.PARTIAL_LINK_TEXT, "Stuff in progress")
    ABANDONED_STUFF = (By.PARTIAL_LINK_TEXT, "Abandoned stuff")
    ABANDONED_LIST = (By.CLASS_NAME, "link-primary")
    NO_STUFF = (By.XPATH, "/html/body/div/p")
    EDIT_STUFF = (By.PARTIAL_LINK_TEXT, "Edit")
    EDIT_PAGE = (By.NAME, "review")
    RATING_ON_EDIT_PAGE = (By.NAME, "rating")
    SAVE_EDIT_PAGE = (
        By.XPATH, '/html/body/div/div/form/input[2]')

    def __init__(self, driver):
        self.driver = driver

    def get_page_title_text(self):
        return self.driver.find_element(*self.PAGE_TITLE).text

    def go_to_your_stuff_page(self):
        self.driver.find_element(*self.PAGE_TITLE).click()

    def go_to_abandoned_stuff_page(self):
        self.driver.find_element(*self.ABANDONED_STUFF).click()

    def go_to_stuff_in_progress_page(self):
        self.driver.find_element(*self.STUFF_IN_PROGRESS).click()

    def get_no_stuff_message(self):
        return self.driver.find_element(*self.NO_STUFF).text

    def verify_abandoned_stuff_page(self):
        return self.driver.find_element(*self.ABANDONED_LIST).is_displayed()

    def go_to_update_stuff_page(self):
        self.driver.find_element(*self.EDIT_STUFF).click()

    def verify_update_stuff_page(self):
        return self.driver.find_element(*self.EDIT_PAGE).is_displayed()

    def add_review_to_stuff(self, review):
        self.driver.find_element(*self.EDIT_PAGE).send_keys(review)

    def save_review_to_stuff(self):
        self.driver.find_element(*self.SAVE_EDIT_PAGE).click()

    def add_rating_to_stuff(self, rating):
        dropdown = Select(self.driver.find_element(
            *self.RATING_ON_EDIT_PAGE))
        dropdown.select_by_visible_text(rating)
