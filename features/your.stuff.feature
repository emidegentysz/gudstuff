Feature: verify my list

Scenario Outline: Verify my stuff page
  Given user is on GoodStuff website
  When user fills in the Sign In form with valid username <username> and valid password <password> and submits it    
  Then user can see homepage
  When user go to Your stuff page
  Then user can see proper page: "Your stuff"
    Examples:
    | username | password |
    | emilia.degentysz@gmail.com | December2022 |

Scenario: Verify my stuff in progress
  When user go to Stuff in progress
  Then user can see proper message: "No stuff in this bucket found." 

Scenario: Verify my Abandoned stuff
  When user go to Your stuff page
  And user go to Abandoned stuff
  Then user can see at least one postion on page

Scenario: Edit position from Want to stuff list
  When user go to Your stuff page
  And user click on edit option
  Then user can see update page
  When user add review: "Ok"
  And user click on save
  Then user can see proper page: "Your stuff" 

Scenario: Edit position rating from Want to stuff list
  When user go to Your stuff page
  And user click on edit option
  Then user can see update page
  When user add rating: "Good"
  And user click on save  
  Then user can see proper page: "Your stuff" 