from selenium import webdriver


def before_feature(context, feature):

    chrome_driver = webdriver.Chrome()
    context.driver = chrome_driver


def after_feature(context, feature):
    context.driver.quit()
