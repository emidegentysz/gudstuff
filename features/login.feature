Feature: sign in to e-mail account
As a user I want to log in
      
Scenario Outline: Log in with valid data
  Given user is on GoodStuff website
  when user fills in the Sign In form with valid username <username> and valid password <password> and submits it
  Then user can see homepage
  Examples:
      | username | password |
      | emilia.degentysz@gmail.com | December2022 |

Scenario: Log out
  when user see Log out button
  Then user click on Log out