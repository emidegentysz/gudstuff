from behave import given, when, then, step  # pylint: disable=no-name-in-module
import pytest
from selenium import webdriver
import sys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from time import sleep
from pages.login import LoginPage


@given('user is on GoodStuff website')
def step_start_page(context):

    context.driver.get('https://gudstuff.herokuapp.com/')
    context.driver.maximize_window()


@when('user fills in the Sign In form with valid username {username} and valid password {password} and submits it')
def step_set_login_in(context, username, password):
    LoginPage(context.driver).enter_username(username)
    LoginPage(context.driver).enter_password(password)
    LoginPage(context.driver).click_ok()


@then('user can see homepage')
def step_homepage(context):

    assert context.driver.find_element(By.CLASS_NAME, 'nav-item')


@when('user see Log out button')
def step_log_out_is_visible(context):
    LoginPage(context.driver).log_out_is_visible()


@step('user click on Log out')
def step_log_out(context):
    LoginPage(context.driver).click_on_log_out()
